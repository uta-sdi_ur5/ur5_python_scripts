# Echo client program
# Important links: 
# https://www.zacobria.com/universal-robots-zacobria-forum-hints-tips-how-to/script-via-socket-connection/
# https://github.com/qian256/ur5_setup
# http://www.me.umn.edu/courses/me5286/robotlab/Resources/scriptManual-3.5.4.pdf


# movel command parameter pose is the x,y,z and the orientation angle from the TCP
# Tool Center Point (TCP). The TCP is the center point of the output flange with the addition of the TCP offset. 

# https://www.universal-robots.com/how-tos-and-faqs/faq/ur-faq/what-is-a-singularity-42311/ --> singularity

import socket
import time
# run arp scan to find address on router 
HOST = '192.168.1.3'
# The remote host
PORT = 30002              # The same port as used by the server
count = 0
while (count < 1):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((HOST, PORT))
    s.send(b'movel(pose_trans(get_forward_kin(),p[0,0,0,0.0,0,0.5]), a=1.2, v=0.25)'+b'\n')
    time.sleep(2)
    #s.send(b'movel(p[.017,.028,.6,0.88,1.68,-1.7504], a=0.1, v=0.25, t=0, r=0)'+b'\n')

    '''time.sleep(0.05)
    s.send (b'set_digital_out(2,True)' + b'\n')
    time.sleep(0.1)
    print('0.2 seconds are up already')
    s.send (b'set_digital_out(7,True)' + b'\n')
    time.sleep(2)
    s.send (b'movej([-0.5405182705025187, -2.350330184112267, -1.316631037266588, -2.2775736604458237, 3.3528323423665642, -1.2291967454894914], a=1.3962634015954636, v=1.0471975511965976)' + b'\n')
    time.sleep(2)
    s.send (b'movej([-0.7203210737806529, -1.82796919039503, -1.8248107684866093, -1.3401161163439792, 3.214294414832996, -0.2722986670990757], a=1.3962634015954636, v=1.0471975511965976)' + b'\n')
    time.sleep(2)
    s.send (b'movej([-0.5405182705025187, -2.350330184112267, -1.316631037266588, -2.2775736604458237, 3.3528323423665642, -1.2291967454894914], a=1.3962634015954636, v=1.0471975511965976)' + b'\n')
    time.sleep(2)
    s.send (b'movej([-0.720213311630304, -1.8280069071476523, -1.8247689994680725, -1.3396385689499288, 3.215063610324356, -0.27251695975573664], a=1.3962634015954636, v=1.0471975511965976)' +b'\n')
    time.sleep(2)
    s.send (b'movej([-0.540537125683036, -2.2018732555807086, -1.0986348160112505, -2.6437150406384227, 3.352864759694935, -1.2294883935868013], a=1.3962634015954636, v=1.0471975511965976)' + b'\n')
    time.sleep(2)
    s.send (b'movej([-0.5405182705025187, -2.350330184112267, -1.316631037266588, -2.2775736604458237, 3.3528323423665642, -1.2291967454894914], a=1.3962634015954636, v=1.0471975511965976)' + b'\n')
    time.sleep(2)
    s.send (b'movej([-0.7203210737806529, -1.570750000000000, -1.570750000000000, -1.3401161163439792, 3.2142944148329960, -0.2722986670990757], t=4.0000000000000000, r=0.0000000000000000)' + b'\n')
    time.sleep(4)
    s.send (b'set_digital_out(2,False)' + b'\n')
    time.sleep(0.05)
    print ('0.2 seconds are up already')
    s.send (b'set_digital_out(7,False)' + b'\n')
    time.sleep(1)'''
    count = count + 1
    print ('The count is:', count)
    time.sleep(1)
data = s.recv(1024)
s.close()
print ('Received', repr(data))
print ('Program finish')