//defines pins
const int stepPin = 6;  //PUL -Pulse
const int dirPin = 7; //DIR -Direction
const int enPin = 8;  //ENA -Enable

void setup(){
  //Sets the pins as Outputs
  pinMode(stepPin,OUTPUT); 
  pinMode(dirPin,OUTPUT);
  pinMode(enPin,OUTPUT);
  digitalWrite(enPin,LOW);
}

void loop(){
  //Enables the motor direction to move
  digitalWrite(dirPin,LOW);
  //Makes 200 Pulses for making one full cycle rotation
  for(int x = 0; x < 3000; x++){
    digitalWrite(stepPin,HIGH); 
    delayMicroseconds(300); 
    digitalWrite(stepPin,LOW); 
    delayMicroseconds(300); 
  }
  
  //One second delay
  delay(1000);
/*
  //Changes the rotations direction
  digitalWrite(dirPin,HIGH);
  // Makes 200 pulses for making one full cycle rotation
  for(int x = 0; x < 2000; x++) {
    digitalWrite(stepPin,HIGH);
    delayMicroseconds(300);
    digitalWrite(stepPin,LOW);
    delayMicroseconds(300);
  }
  */
  //One second delay
  delay(1000); 
}
